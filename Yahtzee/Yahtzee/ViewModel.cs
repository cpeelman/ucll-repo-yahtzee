﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Yahtzee
{
    public class DieViewModel
    {
        private static Random rng = new Random();

        public DieViewModel()
        {
            DieFace = new Cell<int>( RollDie() );
            Keep = new Cell<bool>( false );
        }

        public Cell<int> DieFace
        {
            get;
            private set;
        }

        public Cell<bool> Keep
        {
            get;
            private set;
        }

        public void PerformRoll()
        {
            if ( !Keep.Value )
            {
                DieFace.Value = RollDie();
            }
        }

        private static int RollDie()
        {
            return rng.Next( 6 ) + 1;
        }
    }

    public class DiceRollerViewModel
    {
        private readonly IList<DieViewModel> dice;

        private readonly RollCommand roll;

        public DiceRollerViewModel()
        {
            dice = Enumerable.Range( 1, 5 ).Select( _ => new DieViewModel() ).ToList().AsReadOnly();
            RollsLeft = new Cell<int>( 2 );
            CanRoll = Derived.Create( RollsLeft, n => n > 0 );
            roll = new RollCommand( this );
            DiceRoll = Derived.Create( dice.Select( die => die.DieFace ), ns => new DiceRoll( ns.ToArray() ) );
        }

        public IList<DieViewModel> Dice { get { return dice; } }

        public void PerformRoll()
        {
            RollDice();

            RollsLeft.Value--;
        }

        public ICommand Roll
        {
            get
            {
                return roll;
            }
        }

        public void Reset()
        {
            UnselectDice();
            RollDice();
            this.RollsLeft.Value = 2;
        }

        private void RollDice()
        {
            foreach ( var die in dice )
            {
                die.PerformRoll();
            }
        }

        private void UnselectDice()
        {
            foreach ( var die in dice )
            {
                die.Keep.Value = false;
            }
        }

        public Cell<int> RollsLeft { get; private set; }

        public ICell<bool> CanRoll { get; private set; }

        public ICell<DiceRoll> DiceRoll { get; private set; }

        private class RollCommand : ICommand
        {
            private readonly DiceRollerViewModel viewModel;

            public RollCommand( DiceRollerViewModel viewModel )
            {
                this.viewModel = viewModel;

                viewModel.CanRoll.PropertyChanged += ( sender, args ) =>
                {
                    if ( CanExecuteChanged != null )
                    {
                        CanExecuteChanged( this, new EventArgs() );
                    }
                };
            }

            public bool CanExecute( object parameter )
            {
                return viewModel.CanRoll.Value;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute( object parameter )
            {
                viewModel.PerformRoll();
            }
        }
    }

    public class ScoreLineViewModel
    {
        private readonly ScoreLine scoreLine;

        private readonly DiceRollerViewModel diceRoller;

        public ScoreLineViewModel( ScoreLine scoreLine, DiceRollerViewModel diceRoller )
        {
            this.scoreLine = scoreLine;
            this.diceRoller = diceRoller;
            DiceRollScore = Derived.Create( diceRoller.DiceRoll, scoreLine.Category.Score );
            Assign = new AssignCommand( this );
        }

        public string CategoryName { get { return scoreLine.Category.Name; } }

        public ICell<int> DiceRollScore { get; private set; }

        public ICell<int> AssignedScore
        {
            get
            {
                return scoreLine.Score;
            }
        }

        public ICell<bool> IsAssigned
        {
            get
            {
                return scoreLine.Assigned;
            }
        }

        public ICommand Assign { get; private set; }

        private class AssignCommand : ICommand
        {
            private readonly ScoreLineViewModel scoreLineViewModel;

            public AssignCommand(ScoreLineViewModel scoreLine)
            {
                this.scoreLineViewModel = scoreLine;

                scoreLineViewModel.IsAssigned.PropertyChanged += ( sender, args ) =>
                    {
                        if ( CanExecuteChanged != null )
                        {
                            CanExecuteChanged( this, new EventArgs() );
                        }
                    };
            }

            public bool CanExecute( object parameter )
            {
                return !scoreLineViewModel.IsAssigned.Value;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute( object parameter )
            {
                scoreLineViewModel.scoreLine.Assign( scoreLineViewModel.diceRoller.DiceRoll.Value );
                scoreLineViewModel.diceRoller.Reset();
            }
        }
    }

    public class ScoreSheetViewModel
    {
        private readonly ScoreSheet scoreSheet;

        private readonly IList<ScoreLineViewModel> scoreLines;

        private readonly DiceRollerViewModel diceRoller;

        public ScoreSheetViewModel( ScoreSheet scoreSheet, DiceRollerViewModel diceRoller )
        {
            this.scoreSheet = scoreSheet;
            scoreLines = ( from line in scoreSheet.ScoreLines
                           select new ScoreLineViewModel( line, diceRoller ) ).ToList().AsReadOnly();
            this.diceRoller = diceRoller;
        }

        public IList<ScoreLineViewModel> ScoreLines { get { return scoreLines; } }
    }
}
